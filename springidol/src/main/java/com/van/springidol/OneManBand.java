package com.van.springidol;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class OneManBand implements Performer {
	private Map<String,Instrument> instruments;
	public OneManBand(){}
	
	public void performer() throws PerformanceException {
		// TODO Auto-generated method stub
		Set<Map.Entry<String, Instrument>> entries = instruments.entrySet();
		if(entries != null){
			Iterator it = entries.iterator();
			while(it.hasNext()){
				Map.Entry<String, Instrument> entry = (Entry<String, Instrument>) it.next();
				System.out.print(entry.getKey() + " : ");
				((Instrument)entry.getValue()).play();
			}
		}
	}
	
	public void setInstruments(Map<String,Instrument> instruments){
		this.instruments = instruments;
	}
	
}
