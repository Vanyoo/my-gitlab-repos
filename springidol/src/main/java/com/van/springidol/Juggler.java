package com.van.springidol;

public class Juggler implements Performer {
	private int beanBags = 3;
	
	public Juggler(){
		
	}
	
	public Juggler(int beanBags){
		this.beanBags = beanBags;
	}
	
	public void performer() throws PerformanceException {
		// TODO Auto-generated method stub
		System.out.println("Juggler "+ beanBags + " beanBags");
	}
}
