package com.van.springidol;

public interface Performer {
	public void performer() throws PerformanceException;
}
