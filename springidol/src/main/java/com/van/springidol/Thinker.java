package com.van.springidol;

public interface Thinker {
	public void thinkOfSomething(String thoughts);
}
