package com.van.springidol;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	
	public static void main(String[] args) throws Exception{
	AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("com/van/springidol/spring-idol.xml");
		
		Performer performer1 = (Performer)ctx.getBean("duke");
		Performer performer2 = (Performer)ctx.getBean("poeticDuke");
		Stage theStage = (Stage)ctx.getBean("theStage");
		Auditorium auditorium = (Auditorium)ctx.getBean("auditorium");
		Performer kenny = (Performer)ctx.getBean("kenny");
		Performer hank = (Performer)ctx.getBean("hank");
		//Performer carl = (Performer)ctx.getBean("carl");
		
		try{
			performer1.performer();
			performer2.performer();
			System.out.println(theStage.toString());
			kenny.performer();
			hank.performer();
			//carl.performer();
		}catch(PerformanceException e){
			e.getStackTrace();
		}
		ctx.registerShutdownHook();
	}
}
