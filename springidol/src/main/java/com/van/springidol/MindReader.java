package com.van.springidol;

public interface MindReader {
	public void  interceptThoughts(String thoughts);
	
	String getThoughts();
}
