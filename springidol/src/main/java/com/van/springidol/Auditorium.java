package com.van.springidol;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Auditorium 
		implements InitializingBean, DisposableBean{
	public void turnOnLights(){
		System.out.println("Turn on the lights!");
	}
	
	public void turnOffLights(){
		System.out.println("Turn off the lights.");
	}
	
	public void afterPropertiesSet()
		throws Exception{
		System.out.println("Turn on the lights!");
	}

	public void destroy()
		throws Exception{
		System.out.println("Turn off the lights.");
	}
}
