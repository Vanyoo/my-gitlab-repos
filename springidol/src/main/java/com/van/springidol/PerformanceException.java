package com.van.springidol;

public class PerformanceException extends Exception{
	public PerformanceException(String msg) {
		super(msg);
	}
}
