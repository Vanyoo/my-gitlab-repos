package com.van.springidol;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationBean {
	@Bean
	public Performer duke(){
		return new Juggler(15);
	}
	
	@Bean 
	public Performer kenny(){
		Instrumentalist kenny = new Instrumentalist();
		kenny.setSong("Jingle Bells");
		kenny.setInstrument(piano());
		return kenny;
	}
	
	@Bean
	public Poem sonnet(){
		return new Sonnet();
	}
	
	@Bean
	public Performer poeticDuke(){
		return new PoeticJuggler(sonnet());
	}
	
	@Bean
	public Stage theStage(){
		return Stage.getInstance();
	}
	
	@Bean
	public Auditorium auditorium(){
		return new Auditorium();
	}
	
	@Bean
	public Instrument piano(){
		return new Piano();
	}
	
	@Bean
	public Instrument guitar(){
		return new Guitar();
	}
	
	@Bean
	public Instrument cymbal(){
		return new Cymbal();
	}
	
	@Bean
	public Instrument harmonica(){
		return new Harmonica();
	}
	
	@Bean
	public Performer hank(){
		final Map<String, Instrument> instruments = new HashMap();
		instruments.put("CYMBEL",cymbal());
		instruments.put("HARMONICA", harmonica());
		instruments.put("GUITAR", guitar());
		
		return new OneManBand(){{
			setInstruments(instruments);
		}};
	}
	/*
	@Bean
	public Performer carl(){
		return new Instrumentalist(){{
			setSong("Monk's farm");
			setInstrument(((Instrumentalist)kenny()).getInstrument());
		}};
	}
	*/
	@Bean
	public Audience audience(){
		return new Audience();
	}
}
