package com.van.springidol;

public class PoeticJuggler extends Juggler {
	private Poem poem;
	
	public PoeticJuggler(Poem poem){
		super();
		this.poem = poem;
	}
	
	public PoeticJuggler(int beanBags, Poem poem){
		super(beanBags);
		this.poem = poem;
	}
	
	public void performer() throws PerformanceException{
		super.performer();
		System.out.println("While reciting ...");
		poem.recite();
	}
}
