package com.van.springidol;

public class Instrumentalist implements Performer{
	private String song;
	
	public Instrumentalist(){}
	
	public void performer() throws PerformanceException{
		System.out.print("Playing " + song + " : ");
		instrument.play();
	}
	
	public void setSong(String song){
		this.song = song;
	}
	
	public String getSong(){
		return this.song;
	}
	
	private Instrument instrument;
	
	public void setInstrument(Instrument instrument){
		this.instrument = instrument;
	}
	
	public Instrument getInstrument(){
		return this.instrument;
	}
}
