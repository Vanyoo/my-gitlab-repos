package adaptorPattern;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Created by van on 2/12/16.
 */
public class IteratorEnumration implements Enumeration {
    private Iterator iterator;
    public IteratorEnumration(Iterator iterator) {
        this.iterator = iterator;
    }

    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    public Object nextElement() {
        return iterator.next();
    }
}
