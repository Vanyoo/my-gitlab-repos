package DesignPattern.main;

import DesignPattern.iteratorPattern.Waitress;
import DesignPattern.iteratorPattern.menuClass.DinerMenu;
import DesignPattern.iteratorPattern.menuClass.PancakeHouseMenu;

/**
 * Created by van on 2/17/16.
 */
public class IteratorPatternTest {
    public static void main(String[] args) {
        Waitress waitress = new Waitress();
        waitress.addMenu(new PancakeHouseMenu());
        waitress.addMenu(new DinerMenu());
        waitress.printMenu();
    }
}
