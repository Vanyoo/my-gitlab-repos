package DesignPattern.main;

import DesignPattern.commandPattern.Command;
import DesignPattern.commandPattern.impl.LightOnCommand;
import DesignPattern.commandPattern.util.SimpleRemoteControl;

/**
 * Created by van on 2/7/16.
 */
public class SimpleRemoteControlTest {
    public static void main(String[] args) {
        SimpleRemoteControl remote = new SimpleRemoteControl();
        Command cmd = new LightOnCommand("Garage");
        cmd.execute();
    }
}
