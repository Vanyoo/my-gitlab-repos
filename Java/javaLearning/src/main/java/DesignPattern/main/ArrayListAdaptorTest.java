package DesignPattern.main;

import DesignPattern.adaptorPattern.IteratorEnumration;

import java.util.ArrayList;

/**
 * Created by van on 2/12/16.
 */
public class ArrayListAdaptorTest {
    public static void main(String[] args) {
        ArrayList<Integer> testArrayList = new ArrayList<Integer>();
        testArrayList.add(1);
        testArrayList.add(2);
        testArrayList.add(3);

        IteratorEnumration iteratorEnumration = new IteratorEnumration(testArrayList.iterator());
        while (iteratorEnumration.hasMoreElements()) {
            System.out.println((Integer)iteratorEnumration.nextElement());
        }
    }
}
