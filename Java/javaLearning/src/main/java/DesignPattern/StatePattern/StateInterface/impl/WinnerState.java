package DesignPattern.StatePattern.StateInterface.impl;

import DesignPattern.StatePattern.GumballMachineMachine;
import DesignPattern.StatePattern.StateInterface.State;

/**
 * Created by van on 2/21/16.
 */
public class WinnerState implements State {
    transient private GumballMachineMachine gumballMachine;

    public WinnerState(GumballMachineMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override public String toString() {
        return "�н���!";
    }

    public void insertQuarter() {
        System.out.println("[WinnerState|insertQuarter]�����...");
    }

    public void ejectQuarter() {
        System.out.println("[WinnerState|ejectQuarter]�����...");
    }

    public void turnCrank() {
        System.out.println("[WinnerState|turnCrank]�����...");
    }

    public void dispense() {
        if (gumballMachine.getCount() >= 2) {
            System.out.println("��ϲ��,����10%�����˶�,�������ȡһ���ǹ�!");
            gumballMachine.releaseBall();
            gumballMachine.releaseBall();
            gumballMachine.setState(gumballMachine.getNoQuarterState());
        } else {    // ���<����
            gumballMachine.setState(gumballMachine.getSoldState());
            gumballMachine.getState().dispense();
        }
    }
}
