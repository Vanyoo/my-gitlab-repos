package DesignPattern.StatePattern.StateInterface.impl;

import DesignPattern.StatePattern.GumballMachineMachine;
import DesignPattern.StatePattern.StateInterface.State;

import java.util.Random;

/**
 * Created by van on 2/21/16.
 */
public class HasQuarterState implements State {
    transient private GumballMachineMachine gumballMachine;
    Random randomWinner = new Random(System.currentTimeMillis());

    @Override public String toString() {
        return "已投入25美分.";
    }

    public HasQuarterState(GumballMachineMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    public void insertQuarter() {
        System.out.println("对不起,您已投入25美分,请勿重复投币.");
    }

    public void ejectQuarter() {
        System.out.println("这是您的25美分,请收好.");
        gumballMachine.setState(gumballMachine.getNoQuarterState());
    }

    public void turnCrank() {
        System.out.println("吱嘎吱嘎...");
        int winner = randomWinner.nextInt(10);
        if (winner == 0 && gumballMachine.getCount() > 1) {
            gumballMachine.setState(gumballMachine.getWinnerState());
        } else {
            gumballMachine.setState(gumballMachine.getSoldState());
        }
    }

    public void dispense() {
        System.out.println("[HasQuarterState|dispense]误操作...");
    }
}
