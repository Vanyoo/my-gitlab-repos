package DesignPattern.StatePattern.StateInterface.impl;

import DesignPattern.StatePattern.GumballMachineMachine;
import DesignPattern.StatePattern.StateInterface.State;

/**
 * Created by van on 2/21/16.
 */
public class NoQuarterState implements State {
    transient private GumballMachineMachine gumballMachine;

    public NoQuarterState(GumballMachineMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override public String toString() {
        return "没有投入钱币.";
    }

    public void insertQuarter() {
        System.out.println("您投入了25美分");
        gumballMachine.setState(gumballMachine.getHasQuarterState());
    }

    public void ejectQuarter() {
        System.out.println("对不起,您没有投入钱币,无法取出25美分.");
    }

    public void turnCrank() {
        System.out.println("对不起,您必须先投入25美分才可转动旋钮.");
    }

    public void dispense() {
        System.out.println("[NoQuarterState|dispense]误操作...");
    }
}
