package DesignPattern.StatePattern.StateInterface.impl;

import DesignPattern.StatePattern.GumballMachineMachine;
import DesignPattern.StatePattern.StateInterface.State;

/**
 * Created by van on 2/21/16.
 */
public class SoldOutState implements State {
    transient private GumballMachineMachine gumballMachine;

    public SoldOutState(GumballMachineMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    public void insertQuarter() {
        System.out.println("[SoldOutState|insertQuarter]对不起,糖果已售空.");
    }

    public void ejectQuarter() {
        System.out.println("[SoldOutState|ejectQuarter]对不起,糖果已售空.");
    }

    public void turnCrank() {
        System.out.println("[SoldOutState|turnCrank]对不起,糖果已售空.");
    }

    @Override public String toString() {
        return "糖果已售空";
    }

    public void dispense() {
        System.out.println("[SoldOutState|dispense]对不起,糖果已售空.");
    }
}
