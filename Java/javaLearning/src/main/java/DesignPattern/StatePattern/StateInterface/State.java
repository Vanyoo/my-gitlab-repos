package DesignPattern.StatePattern.StateInterface;

import java.io.Serializable;

/**
 * Created by van on 2/21/16.
 */
public interface State extends Serializable{
    /**
     * 插入25美分
     */
    public void insertQuarter();

    /**
     * 退出25美分
     */
    public void ejectQuarter();

    /**
     * 转动扭杆
     */
    public void turnCrank();

    /**
     * 分配糖果
     */
    public void dispense();
}
