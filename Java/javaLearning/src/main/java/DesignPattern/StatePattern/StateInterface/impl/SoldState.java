package DesignPattern.StatePattern.StateInterface.impl;

import DesignPattern.StatePattern.GumballMachineMachine;
import DesignPattern.StatePattern.StateInterface.State;

/**
 * Created by van on 2/21/16.
 */
public class SoldState implements State{
    public SoldState(GumballMachineMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    transient private GumballMachineMachine gumballMachine;
    public void insertQuarter() {
        System.out.println("您投入25美分.");
        gumballMachine.setState(gumballMachine.getHasQuarterState());
    }

    public void ejectQuarter() {
        System.out.println("[SoldState|ejectQuarter]对不起,钱币已用来购买糖果,如果糖果没有掉落,请联系管理员.");
    }

    @Override public String toString() {
        return "糖果已售出";
    }

    public void turnCrank() {
        System.out.println("[SoldState|turnCrank]对不起,糖果已出售.如果糖果没用掉落,请联系管理员.");
    }

    public void dispense() {
        gumballMachine.releaseBall();
        if (gumballMachine.getCount() > 0) {
            gumballMachine.setState(gumballMachine.getNoQuarterState());
        } else {
            gumballMachine.setState(gumballMachine.getSoldOutState());
        }
    }
}
