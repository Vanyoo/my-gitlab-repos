package DesignPattern.StatePattern;

import DesignPattern.StatePattern.StateInterface.State;
import DesignPattern.StatePattern.StateInterface.impl.*;
import DesignPattern.agencyPattern.GumballMachineRemote;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by van on 2/21/16.
 */
public class GumballMachineMachine extends UnicastRemoteObject implements GumballMachineRemote {
    private State soldOutState;
    private State soldState;
    private State noQuarterState;
    private State hasQuarterState;
    private State winnerState;
    private State state;
    private int count;
    private String location;

    public GumballMachineMachine() throws RemoteException {
        soldOutState = new SoldOutState(this);
        soldState = new SoldState(this);
        noQuarterState = new NoQuarterState(this);
        hasQuarterState = new HasQuarterState(this);
        winnerState = new WinnerState(this);
        state = this.soldOutState;
        count = 0;
    }

    public GumballMachineMachine(int count, String location) throws RemoteException {
        soldOutState = new SoldOutState(this);
        soldState = new SoldState(this);
        noQuarterState = new NoQuarterState(this);
        hasQuarterState = new HasQuarterState(this);
        winnerState = new WinnerState(this);
        if (count > 0) {
            this.count = count;
            state = this.noQuarterState;
        } else {
            count = 0;
            state = this.soldOutState;
        }
    }

    @Override public String toString() {
        StringBuilder result = new StringBuilder(64);
        result.append("---------------------糖果贩卖机---------------------\n当前状态: ")
                .append(this.state.toString()).append("\n剩余糖果: ").append(count)
                .append("\n-----------------------------------------------\n");
        return result.toString();
    }

    public void refill(int number) {
        if (number <= 0) {
            System.out.println("[GumballMachineMachine|addGumballs]误操作...");
        }
        if (count == 0) {
            this.setState(this.getNoQuarterState());
        }
        count += number;
    }

    public void releaseBall() {
        --count;
        System.out.println("给,这是您的糖果.");
    }

    public int getCount() {
        return count;
    }

    public String getLocation() throws RemoteException {
        return location;
    }

    public State getSoldOutState() {
        return soldOutState;
    }

    public State getSoldState() {
        return soldState;
    }

    public State getNoQuarterState() {
        return noQuarterState;
    }

    public State getHasQuarterState() {
        return hasQuarterState;
    }

    public State getWinnerState() {
        return winnerState;
    }

    public State getState() {

        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void insertQuarter() {
        this.state.insertQuarter();
    }

    public void ejectQuarter() {
        this.state.ejectQuarter();
    }

    public void turnCrank() {
        this.state.turnCrank();
        this.state.dispense();
    }
}
