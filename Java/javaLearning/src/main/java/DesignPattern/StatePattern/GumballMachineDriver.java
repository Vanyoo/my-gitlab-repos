package DesignPattern.StatePattern;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

/**
 * Created by van on 2/21/16.
 */
public class GumballMachineDriver {
    public static void main(String[] args) {
        GumballMachineMachine gumballMachine = null;
        if (args.length < 2) {
            System.out.println("GumballMachine <name> <inventory>");
            System.exit(1);
        }
        try {
            gumballMachine = new GumballMachineMachine(Integer.parseInt(args[1]),args[0]);
        } catch (RemoteException e) {
            e.printStackTrace();
            System.out.println("��ʼ��GumballMachine����!");
        }

        try {
            Naming.rebind("//" + gumballMachine.getLocation() + "/gumballMachine", gumballMachine);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }
}
