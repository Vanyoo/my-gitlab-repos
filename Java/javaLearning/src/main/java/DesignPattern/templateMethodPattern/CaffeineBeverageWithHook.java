package DesignPattern.templateMethodPattern;

/**
 * Created by van on 2/13/16.
 */
public abstract class CaffeineBeverageWithHook {
    public abstract void brew();

    public abstract void addCondiments();

    public void pourInCup() {
        System.out.println("Pouring into cup.");
    }

    public void boilWater() {
        System.out.println("Boiling the water.");
    }

    public boolean customerWantsCondiments() {
        return true;
    }

    public final void prepare() {
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()) {
            addCondiments();
        }
    }
}
