package DesignPattern.templateMethodPattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by van on 2/13/16.
 */
public class CoffeeWithHook extends CaffeineBeverageWithHook {

    @Override public void brew() {
        System.out.println("Dripping coffee from filter.");
    }

    @Override public void addCondiments() {
        System.out.println("Adding suger and milk.");
    }

    @Override public boolean customerWantsCondiments() {
        String answer = this.askCustomer();
        if (answer.toLowerCase().startsWith("y")) {
            return true;
        } else {
            return false;
        }
    }

    public String askCustomer() {
        String answer = null;

        System.out.println("Would you like milk and suger with your coffee (y/n)?");

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        try {
            answer = in.readLine();
        } catch (IOException e) {
            System.out.println("I/O exception thrown when reading from BufferedReader.");
        }
        if (answer == null) {
            return "no";
        }
        return answer;
    }
}
