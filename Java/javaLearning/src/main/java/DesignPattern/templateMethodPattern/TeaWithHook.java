package DesignPattern.templateMethodPattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by van on 2/15/16.
 */
public class TeaWithHook extends CaffeineBeverageWithHook {
    @Override public void brew(){
        System.out.println("Dropping tea bag.");
    }

    @Override public void addCondiments() {
        System.out.println("Adding lemon.");
    }

    @Override public boolean customerWantsCondiments() {
        String answer = this.askCustomer();
        if (answer.toLowerCase().startsWith("y")) {
            return true;
        } else {
            return false;
        }
    }

    public String askCustomer() {
        String answer = null;

        System.out.println("Would you like lemon with your tea (y/n)?");

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        try {
            answer = in.readLine();
        } catch (IOException e) {
            System.out.println("I/O exception thrown when reading from BufferedReader.");
        }
        if (answer == null) {
            return "no";
        }
        return answer;
    }

}
