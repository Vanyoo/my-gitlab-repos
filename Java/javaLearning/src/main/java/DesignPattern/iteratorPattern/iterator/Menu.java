package DesignPattern.iteratorPattern.iterator;

import java.util.Iterator;

/**
 * Created by van on 2/20/16.
 */
public interface Menu {
    public Iterator createIterator();
}
