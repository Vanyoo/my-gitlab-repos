package DesignPattern.iteratorPattern.iterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by van on 2/17/16.
 */
public class PancakeHouseMenuIterator implements Iterator {
    private final ArrayList menuList;
    private int position;

    public PancakeHouseMenuIterator(ArrayList menuList) {
        this.menuList = menuList;
        position = 0;
    }

    public Object next() {
        return menuList.get(position++);
    }

    public void remove() {
        if (position == -1) {
            System.out.println("Sorry, there is no item in the menu...");
            return;
        }
        menuList.set(position--, null);
    }

    public boolean hasNext() {
        if (position >= menuList.size() || menuList.get(position) == null) {
            return false;
        }
        return true;
    }
}
