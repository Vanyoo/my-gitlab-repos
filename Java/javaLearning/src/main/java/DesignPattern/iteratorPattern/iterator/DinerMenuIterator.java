package DesignPattern.iteratorPattern.iterator;

import DesignPattern.iteratorPattern.menuClass.MenuItem;

import java.util.Iterator;

/**
 * Created by van on 2/17/16.
 */
public class DinerMenuIterator implements Iterator {
    private final MenuItem[] menuItems;
    private int position;

    public DinerMenuIterator(MenuItem[] menuItems) {
        this.menuItems = menuItems;
        position = 0;
    }

    public void remove() {
        if (position == -1) {
            System.out.println("Sorry, the is no item in the menu...");
        }
        menuItems[position--] = null;
    }

    public boolean hasNext() {
        if (position >= menuItems.length || menuItems[position] == null) {
            return false;
        }
        return true;
    }

    public Object next() {
        return menuItems[position++];
    }
}
