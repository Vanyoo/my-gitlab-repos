package DesignPattern.iteratorPattern;

import DesignPattern.iteratorPattern.iterator.Menu;
import DesignPattern.iteratorPattern.menuClass.MenuItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by van on 2/17/16.
 */
public class Waitress {
    private List<Menu> menus;

    public Waitress() {
        menus = new ArrayList(16);
    }

    public Waitress(List<Menu> menus) {
        menus = menus;
    }

    public void addMenu(Menu menu) {
        menus.add(menu);
    }

    public void printMenu() {
        for (Menu menu : menus) {
            this.printMenu(menu.createIterator());
        }
    }

    public void printMenu(Iterator iterator) {
        while (iterator.hasNext()) {
            ((MenuItem)iterator.next()).printMenuItem();
        }
    }

    public void removeMenu(int index) {
        menus.remove(index);
    }
}
