package DesignPattern.iteratorPattern.menuClass;

import DesignPattern.iteratorPattern.iterator.Menu;
import DesignPattern.iteratorPattern.iterator.PancakeHouseMenuIterator;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by van on 2/17/16.
 */
public class PancakeHouseMenu implements Menu{
    private ArrayList menuItems;

    public PancakeHouseMenu() {
        menuItems = new ArrayList();
        addItem("A", "a", false, 2.99);
        addItem("B", "b", true, 1.99);
        addItem("C", "c", false, 2.89);
        addItem("D", "d", true, 12.99);
    }

    public void addItem(String name, String description, boolean vegetarian, double price) {
        MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
        menuItems.add(menuItem);
    }

    public Iterator createIterator() {
        return new PancakeHouseMenuIterator(menuItems);
    }
}
