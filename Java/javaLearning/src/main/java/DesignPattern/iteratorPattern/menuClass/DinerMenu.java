package DesignPattern.iteratorPattern.menuClass;

import DesignPattern.iteratorPattern.iterator.DinerMenuIterator;
import DesignPattern.iteratorPattern.iterator.Menu;

import java.util.Iterator;

/**
 * Created by van on 2/17/16.
 */
public class DinerMenu implements Menu {
    private final int MAX_MENU_ITEMS = 6;
    private MenuItem[] menuItems;
    private int menuItemsNumber;

    public DinerMenu() {
        this.menuItems = new MenuItem[MAX_MENU_ITEMS];
        addItem("E", "e", false, 0.99);
        addItem("F", "f", true, 2.18);
        addItem("G", "g", true, 4.11);
        addItem("H", "h", false, 5.00);
    }

    public Iterator createIterator() {
        return new DinerMenuIterator(menuItems);
    }

    public void addItem(String name, String description, boolean vegetarian, double price) {
        if (menuItemsNumber >= MAX_MENU_ITEMS) {
            System.out.println("Menu is full,can't add any items.");
            return;
        }
        MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
        menuItems[menuItemsNumber++] = menuItem;
    }
}
