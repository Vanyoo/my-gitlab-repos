package DesignPattern.agencyPattern;

import DesignPattern.StatePattern.StateInterface.State;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by van on 2/22/16.
 */
public interface GumballMachineRemote extends Remote{
    public int getCount() throws RemoteException;

    public String getLocation() throws RemoteException;

    public State getState() throws RemoteException;
}
