package DesignPattern.agencyPattern;

import java.rmi.Naming;

/**
 * Created by van on 2/22/16.
 */
public class GumballMonitorDriver {
    public static void main(String[] args) {
        String[] location = {"rmi://van.yzt/gumballMachine", "rmi://127.0.0.1/gumballMachine",
                "rmi://wuha/gumballMachine"};
        GumballMachineMonitor[] monitors = new GumballMachineMonitor[location.length];

        for(int i =0 ;i<location.length;++i) {
            try {
                GumballMachineRemote machine = (GumballMachineRemote) Naming.lookup(location[i]);
                monitors[i] = new GumballMachineMonitor(machine);
                System.out.println(monitors[i]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        for(int i=0;i<monitors.length;++i) {
            monitors[i].report();
        }
    }
}
