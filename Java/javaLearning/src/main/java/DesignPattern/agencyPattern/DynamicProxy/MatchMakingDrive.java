package DesignPattern.agencyPattern.DynamicProxy;

import DesignPattern.agencyPattern.DynamicProxy.impl.NonOwnerInfomationHandler;
import DesignPattern.agencyPattern.DynamicProxy.impl.OwnerInfomationHandler;
import DesignPattern.agencyPattern.DynamicProxy.impl.PersonBeanImpl;

import java.lang.reflect.Proxy;

/**
 * Created by van on 2/23/16.
 */
public class MatchMakingDrive {
    public static void main(String[] args) {
        MatchMakingDrive matchMakingDrive = new MatchMakingDrive();
        matchMakingDrive.drive();
    }

    public void drive() {
        PersonBean joe = new PersonBeanImpl("joe");
        PersonBean alice = new PersonBeanImpl("Alice");
        PersonBean ownerProxy = this.getOwnerProxy(joe);
        PersonBean nonOwnerProxy = this.getNonOwnerProxy(joe);

        System.out.println("Person :" + ownerProxy.getName());
        try {
            ownerProxy.setHotRating(12);
        } catch (Throwable e) {
            System.out.println("自己不能给自己打分:" + e.getMessage());
        }
        nonOwnerProxy.setHotRating(20);
        System.out.println("Alice 给joe打分后的魅力值:" + ownerProxy.getHotRating());
        nonOwnerProxy.setHotRating(10);
        System.out.println("Taylor 给joe打分后的魅力值:" + ownerProxy.getHotRating());
    }

    public PersonBean getOwnerProxy(PersonBean personBean) {
        return (PersonBean) Proxy.newProxyInstance(personBean.getClass().getClassLoader(),
                personBean.getClass().getInterfaces(), new OwnerInfomationHandler(personBean));
    }

    public PersonBean getNonOwnerProxy(PersonBean personBean) {
        return (PersonBean) Proxy.newProxyInstance(personBean.getClass().getClassLoader(),
                personBean.getClass().getInterfaces(), new NonOwnerInfomationHandler(personBean));
    }
}
