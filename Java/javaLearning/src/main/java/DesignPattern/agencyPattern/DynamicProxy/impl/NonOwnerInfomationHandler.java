package DesignPattern.agencyPattern.DynamicProxy.impl;

import DesignPattern.agencyPattern.DynamicProxy.PersonBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by van on 2/23/16.
 */
public class NonOwnerInfomationHandler implements InvocationHandler {
    private PersonBean personBean;

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            if (method.getName().startsWith("get")) {
                return method.invoke(personBean, args);
            } else if (method.getName().equals("setHotRating")) {
                return method.invoke(personBean, args);
            } else if (method.getName().startsWith("set")) {
                throw new IllegalAccessError("[NonOwnerInfomationHandler]对象不允许设置他人的属性.");
            }
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public NonOwnerInfomationHandler(PersonBean personBean) {
        this.personBean = personBean;
    }
}
