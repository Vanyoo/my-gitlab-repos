package DesignPattern.agencyPattern.DynamicProxy.impl;

import DesignPattern.agencyPattern.DynamicProxy.PersonBean;

/**
 * Created by van on 2/23/16.
 */
public class PersonBeanImpl implements PersonBean {
    private String name;
    private int hotRating;
    private int rateCount;

    public PersonBeanImpl() {
        rateCount = 0;
        hotRating = 0;
    }

    public PersonBeanImpl(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getHotRating() {
        return hotRating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHotRating(int rating) {
        ++rateCount;
        hotRating = (hotRating + rating) / rateCount;
    }
}
