package DesignPattern.agencyPattern.DynamicProxy;

/**
 * Created by van on 2/23/16.
 */
public interface PersonBean {
    String getName();
    int getHotRating();
    void setName(String name);
    void setHotRating(int rating);
}
