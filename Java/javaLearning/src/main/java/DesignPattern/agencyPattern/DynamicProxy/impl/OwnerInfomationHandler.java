package DesignPattern.agencyPattern.DynamicProxy.impl;

import DesignPattern.agencyPattern.DynamicProxy.PersonBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by van on 2/23/16.
 */
public class OwnerInfomationHandler implements InvocationHandler {
    private PersonBean personBean;

    public OwnerInfomationHandler(PersonBean personBean) {
        this.personBean = personBean;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            if (method.getName().startsWith("get")) {
                return method.invoke(personBean, args);
            } else if (method.getName().equals("setHotRating")) {
                throw new IllegalAccessError("[OwnerInfomationHandler]不允许设置自己的hot属性.");
            } else if (method.getName().startsWith("set")) {
                return method.invoke(personBean, args);
            }
        } catch (InvocationTargetException e) {         // 真正主题抛出异常会被这里捕获
            e.printStackTrace();
        }
        return null;
    }
}
