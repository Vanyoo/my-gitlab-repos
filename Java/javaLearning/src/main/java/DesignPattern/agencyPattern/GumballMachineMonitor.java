package DesignPattern.agencyPattern;

import java.rmi.RemoteException;

/**
 * Created by van on 2/22/16.
 */
public class GumballMachineMonitor {
    GumballMachineRemote machine;

    public GumballMachineMonitor(GumballMachineRemote machine) {
        this.machine = machine;
    }

    public void report() {
        try {
            System.out.println("Gumball machine location: " + machine.getLocation());
            System.out.println("Gumball machine count: " + machine.getCount());
            System.out.println("Gumball machine state: " + machine.getState());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
