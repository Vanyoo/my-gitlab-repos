package SynchronizeTest;

/**
 * Created by vanyo on 4/22/16.
 */
public class SynchronizeTest {
    static private final int TEST1_NUM = 1;
    static private final int TEST2_NUM = 2;

    public static void main(String[] args) {
        SynchronizeTestClass synchronizeTestClass = new SynchronizeTestClass();
        synchronizeTestClass.setTestNumber(TEST1_NUM);
        new Thread(synchronizeTestClass).start();
        synchronizeTestClass.setTestNumber(TEST2_NUM);
        new Thread(synchronizeTestClass).start();
    }
}
