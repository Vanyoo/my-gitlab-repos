package SynchronizeTest;

/**
 * Created by vanyo on 4/22/16.
 */
public class SynchronizeTestClass implements Runnable {
    private int testNumber;

    public int getTestNumber() {
        return testNumber;
    }

    public void setTestNumber(int testNumber) {
        this.testNumber = testNumber;
    }

    synchronized public void test1() throws InterruptedException {
        System.out.println("test1 sleep 5 seconds.");
        Thread.sleep(5000);
    }

    synchronized public void test2() throws InterruptedException {
        System.out.println("test2 sleep 5 seconds.");
        Thread.sleep(5000);
    }

    public void run() {
        System.out.println(Thread.currentThread().getName());
        try {
            if (this.testNumber == 1) {
                this.test1();
            } else {
                this.test2();
            }
        } catch (InterruptedException e) {

        }
    }
}
