package commandPattern.util;

import commandPattern.Command;

/**
 * Created by van on 2/7/16.
 */
public class SimpleRemoteControl {
    private Command slot;

    public SimpleRemoteControl(Command command) {
        slot = command;
    }

    public SimpleRemoteControl() {
        slot = null;
    }

    public void setSlot(Command slot) {
        this.slot = slot;
    }

    public void buttonWasPressed() {
        if (slot == null) {
            System.out.println("Command对象没有设置!");
            return;
        }
        slot.execute();
    }
}
