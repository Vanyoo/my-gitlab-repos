package commandPattern.impl;

import commandPattern.Command;

/**
 * Created by van on 2/7/16.
 */
public class LightOnCommand implements Command {
    private String lightName;

    public LightOnCommand(String lightName) {
        this.lightName = lightName;
    }

    public void execute() {
        System.out.println(lightName + "��ƴ�~");
    }
}
