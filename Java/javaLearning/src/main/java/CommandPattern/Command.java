package commandPattern;

/**
 * Created by van on 2/7/16.
 */
public interface Command {
    public void execute();
}
