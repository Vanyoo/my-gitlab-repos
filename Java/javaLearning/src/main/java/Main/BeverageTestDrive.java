package main;

import templateMethodPattern.CoffeeWithHook;
import templateMethodPattern.TeaWithHook;

/**
 * Created by van on 2/15/16.
 */
public class BeverageTestDrive {
    public static void main(String[] args) {
        TeaWithHook teaWithHook = new TeaWithHook();
        CoffeeWithHook coffeeWithHook = new CoffeeWithHook();

        System.out.println("Making tea ...");
        teaWithHook.prepare();

        System.out.println("Making coffee ...");
        coffeeWithHook.prepare();
    }
}
